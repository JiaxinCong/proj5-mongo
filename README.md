Author: Jiaxin Cong
Email: jiaxinc@uoregon.edu
How to operate:
 1. git clone 
 2. open the file "brevets"
 3. build dockerfile
 4. test

NOTE: nose test file was named "test_time.py"

Rules:
 1.if control dist not over 20% of brevets dist, just regard control dist = brevets dist.
 2.if control dist belong to [200,240], close time need to add extra 10 minutes.
 3.if control dist belong to [400,480], close time need to add extra 20 minutes.
 4.other control dist just as the which part they belong to to compute the time.
