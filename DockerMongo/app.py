import os
import arrow
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import logging
import acp_times  # Brevet time calculations
import config

app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
# Step 1: create a client object
# The environment variable DB_PORT_27017_TCP_ADDR is the IP of a linked docker
# container (IP of the database). Generally "DB_PORT_27017_TCP_ADDR" can be set to
# "localhost" or "127.0.0.1".
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)

# Step 2: connect to the DB
db = client.tododb

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route('/todo')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)

# https://www.diffen.com/difference/GET-vs-POST-HTTP-Requests
@app.route('/new', methods=['POST'])
def new():
    km = request.form.getlist("km")
    brevet = request.form.getlist("distance")
    open = request.form.getlist("open")
    close = request.form.getlist("close")

    distance = []
    brevettime = []
    opentime = []
    closetime = []
    if km != None and brevet != None and open != None and close != None:
        for x in km:
            if str(x) != "":
                distance.append(str(x))
        for y in brevet:
            if str(y) != "":
                brevettime.append(str(y))
        for z in open:
            if str(z) != "":
                opentime.append(str(z))
        for n in close:
            if str(n) != "":
                closetime.append(str(n))
    else:
        return redirect(url_for(404))

    data = {'km': distance, 'brevet': brevettime, 'open':opentime, 'close':closetime}
    i = 0
    while i < len(distance):
        result = {"km":data['km'][i], 'brevet': data['brevet'][i], 'open':data['open'][i], 'close':data['close'][i]}
        db.tododb.insert(result)
        i = i+1

    return redirect(url_for('index'))


@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    km = request.args.get('km', 999, type=float)
    brevet_km = request.args.get('brevet_km', 999, type = int)
    brevet_date = request.args.get('brevet_date', 999, type = str)
    brevet_time = request.args.get('brevet_time', 999, type = str)

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    new_time = str(brevet_date) + "T" + str(brevet_time)
    time = arrow.get(new_time)
    if (km > brevet_km*1.2):
        result = {"open": "wrong", "close": "wrong"}
    else:
        open_time = acp_times.open_time(km, brevet_km, time.isoformat())
        close_time = acp_times.close_time(km, brevet_km, time.isoformat())
        result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
